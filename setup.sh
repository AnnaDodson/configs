#!/usr/bin/env bash

## install:
## vim
## git
## i3
## XResource
## tmux
## xterm
## p3status for i3 status bar
## pavucontrol for audio
## font awesome for i3 satus bar
## feh for wallpaper background

sudo apt-get install vim-gtk3 git i3 x11-xserver-utils tmux xterm py3status pavucontrol fonts-font-awesome feh

DIRECTORIES=('vim' 'bash' 'i3' 'tmux' 'xresources')

for dir in "${DIRECTORIES[@]}";
do
	cd $dir
	for file in * .*
	do
		[[ "." == "$file" || ".." == "$file"  || "*" == "$file" ]] && continue
		ln -sf $(readlink -f "$file") ~/$file
	done
	cd ../
done

# Check the directory exists before making the sym link
sudo mkdir -p /etc/X11/xorg.conf.d/
sudo ln -sf $(readlink -f xorg/30-touchpad.conf) /etc/X11/xorg.conf.d/30-touchpad.conf
