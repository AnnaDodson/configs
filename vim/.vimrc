
syntax on
filetype plugin on

"set highlight search on
set hlsearch

"set seaching to incremental search. As type a word, it starts the seach. highlights all words and hit enter to end the search and remove the highlighted words.
set incsearch

"ignore case isgnore case sensitivity 
set ignorecase smartcase

"set the colour scheme 
set bg=dark
color delek
syntax on

"set to show line numbers
set number

"set the folding method per language syntax
"set foldmethod=syntax

"sets the text to display the word line wrapped, not the character
set linebreak

"sets the number to where the curosr is
"set relativenumber 

"set to show commands
set showcmd

"Set showing matching brackets
set showmatch

"Set spelling
au FileType tex set spell spelllang=en
au FileType gitcommit set spell spelllang=en

"Set size of tab
set tabstop=2
set expandtab
set shiftwidth=4

au FileType python set et

"Set to show number of bytes in selected text
vnoremap <C-_> "-y:echo 'text' @- 'has length' strlen(@-)<CR>

"Set git commit messages no more than 72 wide"
au FileType gitcommit set tw=72

"nnoremap <up> <nop>
"nnoremap <down> <nop>
"nnoremap <right> <nop>
"nnoremap <left> <nop>
"inoremap <up> <nop>
"inoremap <down> <nop>
"inoremap <right> <nop>
"inoremap <left> <nop>
