## Dot Files for my Ubuntu laptop

All my personal configs are saved here for easy set up if I want/need to do a clean install.

Install includes vim, tmux and i3 window manager with custom i3 status bar.

Run `$ ./setup.sh` to install everything required and create the symlinks from the repo dotfiles directory to the home directory.

Current colour theme

![space invaders colour scheme][screenshot]

[screenshot]: colour-theme.png